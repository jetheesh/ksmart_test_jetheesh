import React from 'react';
import '../Header/Header.css'

function Header() {
  return (
    <div >
    <h2>Reports</h2>
<p>General New Reports</p>

<div class="row">
  <div class="column">
    <div class="card">
    <i class="fa fa-handshake-o" aria-hidden="true"></i>
      <h3>Grave Sites</h3>
      <button class="button button4">Select</button>
    </div>
  </div>

  <div class="column">
    <div class="card">
    <i class="fa fa-inr" aria-hidden="true"></i>    
      <h3>Payment History</h3>
      <button class="button button4">Select</button>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
    <i class="fa fa-handshake-o" aria-hidden="true"></i>
      <h3>Payment Types</h3>
      <button class="button button4">Select</button>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
    <i class="fa fa-handshake-o" aria-hidden="true"></i>
      <h3>Burial Types</h3>
      <button class="button button4">Select</button>
    </div>
  </div>
  
</div>

<h2>Recently Generated</h2>

<div class="row">
  <div class="column">
    <div class="card">
    <i class="fa fa-file-text-o" aria-hidden="true"></i>
      <h3>Grave Site Report</h3>
      <p>Owner : Mr.Jack </p>
      <p>Date : All</p>
    </div>
  </div>

  <div class="column">
    <div class="card">
    <i class="fa fa-file-text-o" aria-hidden="true"></i>
      <h3>Payment History Report</h3>
      <p>Owner : Mr.Jack </p>
      <p>Date : All</p>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
    <i class="fa fa-file-text-o" aria-hidden="true"></i>
      <h3>Payment Types Report</h3>
      <p>Owner : Mr.Jack </p>
      <p>Date : All</p>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
    <i class="fa fa-file-text-o" aria-hidden="true"></i>
      <h3>Burial Types Report</h3>
      <p>Owner : Mr.Jack </p>
      <p>Date : All</p>
    </div>
  </div>
  
</div>
  </div>
  )
}

export default Header
